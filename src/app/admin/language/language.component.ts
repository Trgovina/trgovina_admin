import { Component, ViewEncapsulation, ViewContainerRef, AfterViewInit, AfterContentInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppConfig } from "../../app.config";
import { Modal } from 'angular2-modal/plugins/bootstrap';
import { ModalModule } from "ng2-modal";
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { LanguageService } from './language.service';
declare var swal: any;
import { PagesComponent } from '../../pages/pages.component';
declare var sweetAlert: any;


@Component({
    selector: 'language',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./language.component.scss'],
    templateUrl: './language.component.html',
    providers: [LanguageService]
})

export class LanguageComponent {
    public rowsOnPage = 10;
    msg = false;
    language: FormGroup;
    editLanguage: FormGroup;
    isRtl = false;
    data = [];
    langId: any;
    obj = '';
    btnFlag = true;
    
    constructor(private _service: LanguageService, private router: Router, vcRef: ViewContainerRef, fb: FormBuilder, public _isAdmin: PagesComponent) {
        this.language = fb.group({
            'languageName': ['', Validators.required],
            'languageCode': ['', Validators.required],
            'isRtl': '',
        });
        this.editLanguage = fb.group({
            'languageName': ['', Validators.required],
            'languageCode': ['', Validators.required],
            'isRtl': '',
        });
    }


    ngOnInit() {

        if (this._isAdmin.isAdmin == false) {
            var role = sessionStorage.getItem('role');
            var roleDt = JSON.parse(role);
            for (var x in roleDt) {
                if (x == 'filter') {
                    if (roleDt[x] == 0) {
                        this.router.navigate(['error']);
                    } else if (roleDt[x] == 100) {
                        jQuery(".buttonCus").hide();
                        jQuery(".thAction").remove();
                        // jQuery(".thTitle").css("width", "61%");
                        this.btnFlag = false;
                    } else if (roleDt[x] == 110) {
                        this.btnFlag = false;
                        jQuery(".thAction").remove();
                        // jQuery(".thTitle").css("width", "61%");
                    }
                }
            }
        }


        this._service.getLangaugeData().subscribe(
            res => {
                // console.log("res", res);
                if (res.data && res.data.length > 0) {
                    this.msg = false;
                    this.data = res.data;
                } else {
                    this.msg = true;
                    this.data = [];
                }
            }
        )
    }

    submitForm(val) {
        this._service.addLanguage(val._value).subscribe(
            res => {
                if (res.code == 200) {
                    jQuery('#addLanguage').modal('hide');
                    this.ngOnInit();
                    this.language.reset();
                    swal("Success!", "Language Added Successfully!", "success");
                } else {
                    sweetAlert("Oops...", "Something went wrong!", "error");
                }
            }
        )
    }

    gotoEditData(data) {
        this.obj = data;
        this.langId = data.langId;
    }
    editForm(val) {
        var x = jQuery("#isRtl").is(":checked");
        val._value.langId = this.langId;
        val._value.isRtl = x;
        this._service.gotoEditLanguage(val._value).subscribe(
            res => {
                if (res.code == 200) {
                    jQuery('#editLanguage').modal('hide');
                    this.ngOnInit();
                    swal("Success!", "Language Added Successfully!", "success");
                } else {
                    sweetAlert("Oops...", res.message, "error");
                }
            }
        )
    }

    gotoDeleteLanguage(lang){
        var cat = this;
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Language!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
            function (isConfirm) {
                if (isConfirm) {
                    cat.deletelanguage(lang);
                    swal({
                        title: 'Delete!',
                        text: 'Language Deleted Successfully!',
                        type: 'success'
                    });

                } else {
                    swal("Cancelled", "Your Language is safe :)", "error");
                }
            });
    }
    deletelanguage(lang) {
         console.log("abc", lang)
        this._service.deleteLanguage(lang.langId).subscribe(
            result => {
                // console.log("result", result);
                // if (result.code != 200) {
                //     sweetAlert("Oops...", "Something went wrong!", "error");
                // }
                // this.dltcategory = result;
                this.ngOnInit();
            }
        )
    }
}