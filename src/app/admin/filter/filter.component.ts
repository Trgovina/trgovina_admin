import { Component, ViewEncapsulation, ViewContainerRef, AfterViewInit, AfterContentInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AppConfig } from "../../app.config";
import { Modal } from 'angular2-modal/plugins/bootstrap';
import { ModalModule } from "ng2-modal";
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { FilterService } from './filter.service';
import { PagesComponent } from '../../pages/pages.component';
import { serializePath } from '@angular/router/src/url_tree';
import { exists } from 'fs';
import { toUnicode } from 'punycode';

declare var swal: any;
declare var sweetAlert: any;
@Component({
    selector: 'filter',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./filter.component.scss'],
    templateUrl: './filter.component.html',
    providers: [FilterService]
})

export class FilterComponent {
    public rowsOnPage = 10;
    msg = true;
    filter: FormGroup;
    editFilter: FormGroup;
    isMandatory = true;
    type = 1;
    data = [];
    langData = [];
    obj = '';
    filterId: any;
    valCount = false;
    btnFlag = true;

    constructor(private route: ActivatedRoute, private _service: FilterService, fb: FormBuilder, private router: Router, vcRef: ViewContainerRef, public _isAdmin: PagesComponent) {
        this.filter = fb.group({
            'fieldName': ['', Validators.required],
            'type': ['', Validators.required],
            'values': ['1'],
            'valuesOther': ['1'],
            'fieldNameOther': ['', Validators.required],
            'isMandatory': 'true',
        });
        this.editFilter = fb.group({
            'newFieldName': ['', Validators.required],
            'type': ['', Validators.required],
            'values': [''],
            'isMandatory': '',
        });
    }

    ngOnInit() {
        if (this._isAdmin.isAdmin == false) {
            var role = sessionStorage.getItem('role');
            var roleDt = JSON.parse(role);
            for (var x in roleDt) {
                if (x == 'filter') {
                    if (roleDt[x] == 0) {
                        this.router.navigate(['error']);
                    } else if (roleDt[x] == 100) {
                        jQuery(".buttonCus").hide();
                        jQuery(".thAction").remove();
                        // jQuery(".thTitle").css("width", "61%");
                        this.btnFlag = false;
                    } else if (roleDt[x] == 110) {
                        this.btnFlag = false;
                        jQuery(".thAction").remove();
                        // jQuery(".thTitle").css("width", "61%");
                    }
                }
            }
        }

        jQuery("#addDiv").hide();

        this._service.getData().subscribe(
            res => {
                if (res.data && res.data.length > 0) {
                    this.data = res.data;
                    this.msg = false;
                } else {
                    this.msg = true;
                    this.data = [];
                }
            }
        )


        this._service.getLanguage().subscribe(
            res => {
                if (res.data && res.data.length > 0) {
                    this.langData = res.data;
                } else {
                    this.langData = [];
                }
            }
        )

    }
    checkValues(s) {
        if (s == 1 || s == 3 || s == 5 || s == 7) {
            jQuery("#addDiv").hide();
            jQuery("#editDiv").hide();
        } else {
            jQuery("#addDiv").show();
            jQuery("#editDiv").show();
        }
    }

    submitForm(val) {
        console.log("valaaaaaaaaaaaaaa", val)
        var otherName = [];
        // console.log("otherName", otherName);
        // if (jQuery('.values').val()) {
        //     for (let i = 0; i < this.langData.length; i++) {
        //         if (!jQuery(".addValues" + this.langData[i].langId).val()) {
        //             sweetAlert("Oops...", "Please fill " + this.langData[i].langaugeName + " values", "error");
        //             // alert("please fill" + this.langData[i].langaugeName);
        //             // this.valCount = false;
        //             break;
        //         } else {
        //             let x = {
        //                 langId: this.langData[i].langId,
        //                 fieldName: jQuery(".addField" + this.langData[i].langId).val(),
        //                 values: jQuery(".addValues" + this.langData[i].langId).val()
        //             }
        //             otherName.push(x);
        //         }
        //         // this.valCount = true;
        //     }
        // }
        // if (jQuery('.values').val()) {
        //     if (otherName.length == this.langData.length) {
        //         val._value.otherName = otherName;
        //         this._service.addFilter(val._value).subscribe(
        //             res => {
        //                 if (res.code == 200) {
        //                     swal("Success!", "Added Successfully!", "success");
        //                     jQuery('#addFilter').modal('hide');
        //                     this.filter.reset();
        //                     jQuery(".typeVal").val(1);
        //                     jQuery('#mandVal').prop('checked', true);
        //                     this.type = 1;
        //                     this.isMandatory = true;
        //                     this.ngOnInit();
        //                 } else {
        //                     sweetAlert("Oops...", "Something went wrong!", "error");
        //                 }
        //             }
        //         )
        //     }
        // } else {
        //     val._value.otherName = otherName;
        //     val._value.type = jQuery(".typeVal").val();
        //     val._value.isMandatory = jQuery('#mandVal').is(':checked');
        //     this._service.addFilter(val._value).subscribe(
        //         res => {
        //             if (res.code == 200) {
        //                 swal("Success!", "Added Successfully!", "success");
        //                 jQuery('#addFilter').modal('hide');
        //                 this.filter.reset();
        //                 // jQuery(".typeVal").val(1);
        //                 jQuery('#mandVal').prop('checked', true);
        //                 // this.type = 1;
        //                 this.isMandatory = true;
        //                 this.ngOnInit();
        //             } else {
        //                 sweetAlert("Oops...", "Something went wrong!", "error");
        //             }
        //         }
        //     )
        // }
        // this.langData.forEach(e => {
        //     let x = {
        //         langId: e.langId,
        //         fieldName: jQuery(".addField" + e.langId).val(),
        //         values: jQuery(".addValues" + e.langId).val()
        //     }
        //     otherName.push(x);
        // });

        for (let i = 0; i < this.langData.length; i++) {

            let x = {
                langId: this.langData[i].langId,
                fieldName: jQuery(".addField" + this.langData[i].langId).val(),
                values: jQuery(".addValues" + this.langData[i].langId).val()
            }
            otherName.push(x);

        }
        val._value.otherName = otherName;
        console.log("submite post", val._value);
        this._service.addFilter(val._value).subscribe(
            res => {
                if (res.code == 200) {
                    swal("Success!", "Added Successfully!", "success");
                    jQuery('#addFilter').modal('hide');
                    this.filter.reset();
                    this.type = 1;
                    this.ngOnInit();
                } else {
                    sweetAlert("Oops...", "Something went wrong!", "error");
                }
            }
        )
    }

    gotoEditData(data) {
        jQuery(".eField").val('');
        jQuery(".eValues").val('');
        this.type = data.type;
        this.obj = data;
        this.filterId = data.filterId;
        this.langData.forEach(ele => {
            data.otherName.forEach(element => {
                if (ele.langId == element.langId) {
                    jQuery(".editField" + ele.langId).val(element.fieldName);
                    jQuery(".editValues" + ele.langId).val(element.values);
                }
            });
        });
        if (this.type == 1 || this.type == 3 || this.type == 5) {
            jQuery("#editDiv").hide();
        } else {
            jQuery("#editDiv").show();
        }
    }

    editFilterFunc(val) {
        var otherName = [];
        if (jQuery('.editValues').val()) {
            for (let i = 0; i < this.langData.length; i++) {
                if (!jQuery(".editValues" + this.langData[i].langId).val()) {
                    sweetAlert("Oops...", "Please fill " + this.langData[i].langaugeName + " values", "error");
                    // alert("please fill" + this.langData[i].langaugeName);
                    // this.valCount = false;
                    break;
                } else {
                    let x = {
                        langId: this.langData[i].langId,
                        fieldName: jQuery(".editField" + this.langData[i].langId).val(),
                        values: jQuery(".editValues" + this.langData[i].langId).val()
                    }
                    otherName.push(x);
                }
                // this.valCount = true;
            }
        }
        val._value.otherName = otherName;
        val._value.filterId = this.filterId;
        // console.log("val._value", val._value);
        if (jQuery('.values').val()) {
            if (otherName.length == this.langData.length) {
                this._service.editFilter(val._value).subscribe(
                    res => {
                        if (res.code == 200) {
                            swal("Success!", "Edited Successfully!", "success");
                            jQuery('#editFilter').modal('hide');
                            this.editFilter.reset();
                            this.type = 1;
                            this.ngOnInit();
                        } else {
                            sweetAlert("Oops...", "Something went wrong!", "error");
                        }
                    }
                )
            }
        } else {
            this.langData.forEach(e => {
                let x = {
                    langId: e.langId,
                    fieldName: jQuery(".editField" + e.langId).val(),
                    values: jQuery(".editValues" + e.langId).val()
                }
                otherName.push(x);
            });
            //val._value.isMandatory = jQuery('#mandValedit').is(':checked');

            console.log("val.", val._value);

            this._service.editFilter(val._value).subscribe(
                res => {
                    if (res.code == 200) {
                        swal("Success!", "Edited Successfully!", "success");
                        jQuery('#editFilter').modal('hide');
                        this.editFilter.reset();
                        this.type = 1;
                        this.ngOnInit();
                    } else {
                        sweetAlert("Oops...", "Something went wrong!", "error");
                    }
                }
            )
        }



    }

    gotoDeleteFilter(filterId) {
        var id = this;
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Filter!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
            function (isConfirm) {
                if (isConfirm) {
                    id.deleteFilter(filterId);
                    swal({
                        title: 'Delete!',
                        text: 'Filter Deleted Successfully!',
                        type: 'success'
                    });

                } else {
                    swal("Cancelled", "Your Filter is safe :)", "error");
                }
            });
    }
    deleteFilter(filterId) {
        console.log("filterId", filterId);
        this._service.deleteFilter(filterId).subscribe(
            res => {
                // console.log("res", res);
                this.ngOnInit();
            }
        )
    }
}