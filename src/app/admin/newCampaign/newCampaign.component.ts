import { Component, ViewEncapsulation, ViewContainerRef, AfterViewInit, AfterContentInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AppConfig } from "../../app.config";
import { Modal } from 'angular2-modal/plugins/bootstrap';
import { ModalModule } from "ng2-modal";
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { NewCampaignService } from './newCampaign.service';
import { IMyDpOptions, IMyDateModel } from 'mydatepicker';
import { PagesComponent } from '../../pages/pages.component';

declare var swal: any;
declare var sweetAlert: any;
@Component({
    selector: 'newCampaign',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./newCampaign.component.scss'],
    templateUrl: './newCampaign.component.html',
    providers: [NewCampaignService]
})

export class NewCampaignComponent {
    notificationM: FormGroup;
    autocomplete: any;
    place: any;
    lat: any;
    long: any;
    cityName: any;
    cautocomplete: any;
    cplace: any;
    clat: any;
    clong: any;
    campaignType = 'city';
    unitType = 'KM';
    errMsg = '';
    userPush = [];
    userData = [];
    msg = true;
    public count: any;
    rowsOnPage = 30;
    public p = 1;
    detailData: any;

    pMsg = 0;
    public images: string[] = [];
    pushImgUrl = 'http://185.28.3.188/public/defaultImg.png';
    pushTitle: any;
    pushMsg: any;
    pushUrl: any;
    cdate: any;
    model: any;
    nDate: any;
    startDate: any;
    endDate: any;
    Dimage: any = '';


    private myDatePickerOptions: IMyDpOptions = {
        // other options...
        todayBtnTxt: 'Today',
        dateFormat: 'yyyy-mm-dd',
        firstDayOfWeek: 'mo',
        sunHighlight: true,
        inline: false,
        disableUntil: { year: 2017, month: 8, day: 10 }
    };

    // Initialized to specific date (09.10.2018).
    constructor(private route: ActivatedRoute, private _service: NewCampaignService, private router: Router, vcRef: ViewContainerRef, fb: FormBuilder, private changeDetectorRef: ChangeDetectorRef, public _isAdmin: PagesComponent) {
        this.notificationM = fb.group({
            // "type": ['', Validators.required],
            "line2": ['', Validators.required],
            "message": ['', Validators.required],
            "image": [''],
            "url": [''],
            // "numberOfUser": ['']
        });
    }

    ngOnInit() {
        jQuery("#sendBtn").hide();
        if (this._isAdmin.isAdmin == false) {
            var role = sessionStorage.getItem('role');
            var roleDt = JSON.parse(role);
            for (var x in roleDt) {
                if (x == 'campaign') {
                    if (roleDt[x] == 0) {
                        this.router.navigate(['error']);
                    }
                }
            }
        }

        jQuery("#countryTxt").hide(); jQuery("#tableDiv").hide(); jQuery("#uploadBtn").prop('disabled', true);
        jQuery("#upImg").hide();
        jQuery("#loader").hide();


        // this.p = 1;
        this.cdate = new Date();
        var day = this.cdate.getUTCDate();
        var month = this.cdate.getUTCMonth() + 1;
        var year = this.cdate.getUTCFullYear();
        this.model = { date: { year: year, month: month, day: day } };

        var nday = this.cdate.getUTCDate() + 2;
        var nmonth = this.cdate.getUTCMonth() + 1;
        var nyear = this.cdate.getUTCFullYear();
        this.nDate = { date: { year: nyear, month: nmonth, day: nday } };


        this.autocomplete = new google.maps.places.Autocomplete((<HTMLInputElement>document.getElementById('cityTxt')), { types: ['(cities)'] });
        this.autocomplete.addListener('place_changed', () => {
            this.place = this.autocomplete.getPlace();
            this.lat = this.place.geometry.location.lat();
            this.long = this.place.geometry.location.lng();
            for (var i = 0; i < this.place.address_components.length; i += 1) {
                var addressObj = this.place.address_components[i];
                for (var j = 0; j < addressObj.types.length; j += 1) {
                    if (addressObj.types[j] === 'locality') {
                        this.cityName = addressObj.long_name;
                    }
                }
            }
        });


        this.cautocomplete = new google.maps.places.Autocomplete((<HTMLInputElement>document.getElementById('countryTxt')));
        this.cautocomplete.addListener('place_changed', () => {
            this.cplace = this.cautocomplete.getPlace();
            this.clat = this.cplace.geometry.location.lat();
            this.clong = this.cplace.geometry.location.lng();
            for (var i = 0; i < this.cplace.address_components.length; i += 1) {
                var addressObj = this.cplace.address_components[i];
                for (var j = 0; j < addressObj.types.length; j += 1) {
                    if (addressObj.types[j] === 'country') {
                        this.cityName = addressObj.long_name;
                    }
                }
            }
        });
    }
    radioBtn() {
        if (this.campaignType == "city") {
            jQuery("#cityTxt").show();
            jQuery("#countryTxt").hide();
            jQuery("#countryTxt").val('');
        } if (this.campaignType == "country") {
            jQuery("#cityTxt").hide();
            jQuery("#cityTxt").val('');
            jQuery("#countryTxt").show();
        }
    }

    onDateChanged(event: IMyDateModel) {
        this.startDate = event.epoc;
    }
    onDateChangedEnd(event: IMyDateModel) {
        this.endDate = event.epoc;
    }

    goForTargetUser1() {
        // console.log("startDate", this.startDate);
        // console.log("endDate", this.endDate);
        if (this.startDate == undefined || this.endDate == undefined) {
            this.errMsg = 'Date';
        } else {
            this.goForTargetUser(this.p);
        }
    }
    goForTargetUser(p) {
        jQuery("#loader").show();
        if (this.campaignType == "city") {
            var campTitle = jQuery("#campTitle").val();
            var city = jQuery("#cityTxt").val();
            var radius = jQuery("#radius").val();
            if (campTitle.length == 0) {
                this.errMsg = 'Campaign Title'
            } else if (city.length == 0) {
                this.errMsg = 'City Name';
            } else if (radius.length == 0) {
                this.errMsg = 'Radius';
            } else {
                jQuery("#err").hide();
                this._service.getTargetUser(p - 1, this.rowsOnPage, this.lat, this.long, this.cityName, radius, this.unitType).subscribe(
                    res => {
                        this.userPush = [];
                        this.count = 0;
                        if (res.code == 200) {
                            jQuery("#loader").hide();
                            if (res.data && res.data.length > 0) {
                                jQuery("#tableDiv").show();
                                jQuery("#sendBtn").show();
                                this.msg = false;
                                this.userData = res.data;
                                this.count = res.count;
                                this.p = p;
                                this.userData.forEach(element => {
                                    var users = {
                                        pushToken: element.pushToken,
                                        userId: element.userId,
                                        username: element.username,
                                        phoneNumber: element.phoneNumber,
                                        city: element.city
                                    }
                                    this.userPush.push(users);
                                });
                            } else {
                                this.msg = true;
                                this.userPush = [];
                                jQuery("#tableDiv").show(); jQuery("#sendBtn").hide(); jQuery("#loader").hide();
                            }
                        } else {
                            jQuery("#tableDiv").show(); jQuery("#sendBtn").hide(); jQuery("#loader").hide();
                            this.msg = true;
                            this.userPush = [];
                        }
                    }
                )
            }
        } else if (this.campaignType == "country") {
            var campTitle = jQuery("#campTitle").val();
            var country = jQuery("#countryTxt").val();
            var radius = jQuery("#radius").val();
            if (campTitle.length == 0) {
                this.errMsg = 'Campaign Title'
            } else if (country.length == 0) {
                this.errMsg = 'Country Name';
            } else if (radius.length == 0) {
                this.errMsg = 'Radius';
            } else {
                jQuery("#err").hide();
                this._service.getTargetUser(p - 1, this.rowsOnPage, this.clat, this.clong, this.cityName, radius, this.unitType).subscribe(
                    res => {
                        this.userPush = []; this.count = 0;
                        if (res.code == 200) {
                            jQuery("#loader").hide();
                            if (res.data && res.data.length > 0) {
                                jQuery("#tableDiv").show();
                                jQuery("#sendBtn").show();
                                this.msg = false;
                                this.userData = res.data;
                                this.count = res.count;
                                this.p = p;
                                this.userData.forEach(element => {
                                    var users = {
                                        pushToken: element.pushToken,
                                        userId: element.userId,
                                        username: element.username,
                                        phoneNumber: element.phoneNumber,
                                        city: element.city
                                    }
                                    this.userPush.push(users);
                                });
                            } else {
                                this.msg = true;
                                this.userPush = [];
                                jQuery("#tableDiv").show(); jQuery("#sendBtn").hide(); jQuery("#loader").hide();
                            }
                        } else {
                            jQuery("#tableDiv").show(); jQuery("#sendBtn").hide(); jQuery("#loader").hide();
                            this.msg = true;
                            this.userPush = [];
                        }
                    }
                )
            }
        }
    }
    sendNotification() {
        var campTitle = jQuery("#campTitle").val();
        var title = jQuery("#title").val();
        var msg = jQuery("#message").val();
        var url = jQuery("#urlLink").val();
        var url2 = 'http://' + url;
        if (this.userPush.length == 0) {
            swal("No User Found");
        } else {
            this._service.sendCampaign(campTitle, this.userPush, this.pushImgUrl, title, msg, url2, this.cityName, this.campaignType, this.startDate, this.endDate).subscribe(
                res => {
                    // console.log("res", res);
                    // if (res.code == 200) {
                        jQuery('#sendPush').modal('hide');
                        jQuery("#subDiv").show();
                        this.notificationM.reset();
                        this.userData = [];
                        this.ngOnInit();
                        this.userPush = [];
                        this.pMsg = 0;
                        this.pushImgUrl = 'http://185.28.3.188/public/defaultImg.png';
                        this.pushTitle = '';
                        this.Dimage = '';
                        this.pushMsg = '';
                        this.pushUrl = '';
                        swal("Success!", "Campaign Sent Successfully!", "success");
                    // } else {
                    //     sweetAlert("Oops...", "Something went wrong!", "error");
                    // }
                }
            )
        }
    }

    DfileChange(input) {
        // alert(1);
        if ((input.files[0].size / 1000) > 300) {
            jQuery("#upImg").show();
            jQuery("#uploadBtn").prop('disabled', true);
        } else {
            jQuery("#uploadBtn").prop('disabled', false);
            jQuery("#upImg").hide();
            const reader = new FileReader();
            if (input.files.length) {
                const file = input.files[0];
                reader.onload = () => {
                    this.Dimage = reader.result;
                }
                reader.readAsDataURL(file);
            }
        }
    }
    DremoveImage(): void {
        jQuery("#uploadBtn").prop('disabled', true);
        this.Dimage = '';
    }
    confirmUploadPush() {
        jQuery("#imgLoader").css("display", "block");
        this._service.uploadPushImage(this.Dimage).subscribe(
            res => {
                if (res.code == 200) {
                    this.pushImgUrl = res.data;
                    jQuery("#imgLoader").css("display", "none");
                }
            }
        )
    }
    pushUsers(value) {
        if (value._value.line2.length > 0 && value._value.message.length > 0) {
            this.pMsg = 1;
            this.pushTitle = value._value.line2;
            this.pushMsg = value._value.message;
            this.pushUrl = value._value.url;
            jQuery('#detailsBoxPush').modal('hide');

        } else {
            this.pMsg = 0;
        }
    }
    closeModal() {
        jQuery('#sendPush').modal('hide');
    }

    gotoUserDatail(user) {
        this._service.getUserDetail(user).subscribe(
            result => {
                if (result.code == 200) {
                    this.detailData = result.data;
                    jQuery('#userDetail').modal('show');
                }
            }
        )
    }
    setDefaultImg() {
        this.pushImgUrl = "http://185.28.3.188/public/defaultImg.png";
    }

}
