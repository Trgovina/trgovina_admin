import { Injectable } from '@angular/core';
import { HttpModule, Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Configuration } from '../app.constant';

@Injectable()
export class CategoriesService {
    public Url: string;
    public headers = new Headers({ 'Content-Type': 'application/json' });
    public options = new RequestOptions({
        headers: this.headers
    });
    constructor(public http: Http, public _config: Configuration) { }

    getCategory() {
        let Url = this._config.Server + "getCategories";
        // let Url = this._config.Server + "getCategories";
        return this.http.get(Url, { headers: this._config.headers }).map(res => res.json());
        
    }
    deletecategory(category): Observable<any> {
        let url = this._config.Server + "removeCategory";
        // let url = this._config.Server + "removeCategory";
        let body = JSON.stringify(category);
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }
    deleteDeActivecategory(deActiveCategorieId): Observable<any> {
        let url = this._config.Server + "deActiveCategories/"+"?deActiveCatId=" + deActiveCategorieId.categoryNodeId+"&image="+deActiveCategorieId.activeimage;
        // let body = JSON.stringify(deActiveCategorieId);
        return this.http.delete(url, { headers: this._config.headers }).map(res => res.json());
    }
    addCategory(categories): Observable<any> {
        console.log("server----",categories)
        let url = this._config.Server + "adminCategory";
        let body = JSON.stringify(categories);
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }
    editCategory(oldName, newName, activeImg, Aimage, otherName): Observable<any> {
        // let url = this._config.Server + "adminCategory";
        let url = this._config.Server + "adminCategory";
        let body = {
            oldName: oldName,
            newName: newName,
            activeImg: activeImg,
            activeimage: Aimage,
            otherName: otherName
        };
        // console.log("body", body);
        return this.http.put(url, body, { headers: this._config.headers }).map(res => res.json());

    }
    imgUpload(data): Observable<any> {
        let url = this._config.Server + "fileUpload";
        let body = { uploadedImages: data };
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }

    getLanguage() {
        let url = this._config.Server + 'language';
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getFields(name, opType) {
        let Url = this._config.Server + "fields/" + name + "?type=" + opType;
        return this.http.get(Url, { headers: this._config.headers }).map(res => res.json());
    }
    getFilter() {
        let url = this._config.Server + 'filter';
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    addFields(fields, name): Observable<any> {
        let url = this._config.Server + "fields/" + name;
        let body = { filterId: fields.filterId, type: fields.type };
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }
    editField(val, name): Observable<any> {
        let url = this._config.Server + "fields/" + name;
        let body = JSON.stringify(val);
        return this.http.put(url, body, { headers: this._config.headers }).map(res => res.json());
    }
    deletefields(filterId, name, opType): Observable<any> {
        let url = this._config.Server + "fields/" + name + "?filterId=" + filterId + "&type=" + opType;
        return this.http.delete(url, { headers: this._config.headers }).map(res => res.json());
    }

    getDeActiveCat() {
        let url = this._config.Server + "deActiveCategories";
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    gotoActiveCategory(categoryName) {
        let url = this._config.Server + "activeCategory";
        return this.http.post(url, { categoryName: categoryName }, { headers: this._config.headers }).map(res => res.json());
    }
    gotoRowsUpate(catId, otherId, type) {
        let url = this._config.Server + "reorderCategory";
        let body = {
            currId: catId,
            otherId: otherId,
            type: type
        };
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }
}

