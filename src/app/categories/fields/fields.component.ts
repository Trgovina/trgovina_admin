import { Component, OnInit, OnDestroy, ViewEncapsulation, ViewContainerRef, Input, AfterViewInit, AfterContentInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Modal } from 'angular2-modal/plugins/bootstrap';
import { ModalModule } from "ng2-modal";
import { fieldsService } from './fields.service';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary'
import { PagesComponent } from '../../pages/pages.component';

declare var swal: any;
declare var sweetAlert: any;
declare var CLOUDNAME: string, CLOUDPRESET: string;

@Component({
    selector: 'fields',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./fields.component.scss'],
    templateUrl: './fields.component.html',
    providers: [fieldsService]

})

export class fieldscomponent implements OnInit {
    sub: any;
    subCategoryName: any;
    msg: any = false;
    fieldData: any;
    public rowsOnPage = 10;
    addFields: FormGroup;
    editFields: FormGroup;
    fieldType = "1";
    isMandatory = true;
    public obj = '';
    oldFilterId: any;
    newFields: any;
    public url: any;
    fieldName: any;
    state = 1;
    errorImg = false;
    opType = 2;
    filterData = [];
    btnFlag = true;

    uploader: CloudinaryUploader = new CloudinaryUploader(
        new CloudinaryOptions({ cloudName: CLOUDNAME, uploadPreset: CLOUDPRESET })
    );



    constructor(private route: ActivatedRoute, private _fieldsService: fieldsService, private router: Router, vcRef: ViewContainerRef, fb: FormBuilder, public _isAdmin: PagesComponent) {
        this.addFields = fb.group({
            // 'fieldName': ['', Validators.required],
            'filterId': ['', Validators.required],
            // 'isMandatory': ['', Validators.required],

        });
        this.editFields = fb.group({
            // 'newFieldName': ['', Validators.required],
            'filterId': '',
            // 'isMandatory': '',
        });

    }
    upload() {
        this.uploader.uploadAll();
        let photo = jQuery("#photo").val();
        if (photo) {
            this.errorImg = false;
            this.uploader.uploadAll();
        } else {
            this.errorImg = true;
        }
    }
    ngOnInit() {
        if (this._isAdmin.isAdmin == false) {
            var role = sessionStorage.getItem('role');
            var roleDt = JSON.parse(role);
            for (var x in roleDt) {
                if (x == 'categories') {
                    if (roleDt[x] == 0) {
                        this.router.navigate(['error']);
                    } else if (roleDt[x] == 100) {
                        jQuery(".buttonCus").hide();
                        jQuery(".thAction").remove();
                        // jQuery(".thTitle").css("width", "61%");
                        this.btnFlag = false;
                    } else if (roleDt[x] == 110) {
                        this.btnFlag = false;
                        jQuery(".thAction").remove();
                        // jQuery(".thTitle").css("width", "61%");
                    }
                }
            }
        }


        this.sub = this.route.params.subscribe(params => {
            this.subCategoryName = params['subCategoryName'];
        });
        this._fieldsService.getFilter().subscribe(
            res => {
                // console.log("filter", res);
                if (res.data && res.data.length > 0) {
                    this.filterData = res.data;
                    console.log("this.filterData = res.data;", this.filterData)
                } else {
                    this.fieldData = [];
                }
            }
        )
        this._fieldsService.getFields(this.subCategoryName, this.opType).subscribe(
            result => {
                // console.log("result", result);
                if (result.data && result.data.length > 0) {
                    this.msg = false;
                    this.fieldData = result.data;
                } else {
                    this.msg = true;
                    this.fieldData = [];
                }
            }
        )

    }
    submitForm(value) {
        console.log("val", value._value);
        value._value.type = this.opType;
        this._fieldsService.addFields(value._value, this.subCategoryName).subscribe(
            result => {
                if (result.code == 200) {
                    swal("Success!", "Field Added Successfully!", "success");
                    jQuery('#addFields').modal('hide');
                    this.addFields.reset();
                    this.ngOnInit();

                } else {
                    sweetAlert("Oops...", "Something went wrong!", "error");
                }
            }
        )
    }

    gotoeditFields(fields) {
        // console.log("fields", fields);
        // this.obj = fields.isMandatory;
        // // this.type = fields.type;
        this.oldFilterId = fields.filterId;

        jQuery('#editFilterType').val(fields.filterId);

    }
    updatecategory(val) {
        val._value.oldFilterId = this.oldFilterId;
        // console.log("d", val._value);
        val._value.type = this.opType;
        this._fieldsService.editField(val._value, this.subCategoryName.trim()).subscribe(
            result => {
                if (result.code == 200) {
                    jQuery('#editFields').modal('hide');
                    this.ngOnInit();
                    swal("Success!", "Updated Successfully!", "success");
                } else {
                    sweetAlert("Oops...", "Something went wrong!", "error");
                }
            }
        )

    }

    gotoDeleteFields(fields) {
        // console.log("delete is ", fields);
        var cat = this;
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this field!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
            function (isConfirm) {
                if (isConfirm) {
                    cat.deletesubcategory(fields);
                    swal({
                        title: 'Delete!',
                        text: 'field Deleted Successfully!',
                        type: 'success'
                    });

                } else {
                    swal("Cancelled", "Your field is safe :)", "error");
                }
            });
    }
    deletesubcategory(fields) {
        this._fieldsService.deletefields(fields, this.subCategoryName.trim(), this.opType).subscribe(
            result => {
                this.ngOnInit();
            }
        )
    }

    gotoUpArrow(index) {
        if (index > 0) {
            const tmp = this.fieldData[index - 1];
            this.fieldData[index - 1] = this.fieldData[index];
            this.fieldData[index] = tmp;
            this._fieldsService.gotoRowsUpate(this.fieldData[index].filId, this.fieldData[index - 1].filId, 3).subscribe(
                res => {
                    this.ngOnInit();
                }
            )
        }
    }
    gotoDownArrow(index) {
        if (index < this.fieldData.length) {
            const tmp = this.fieldData[index + 1];
            this.fieldData[index + 1] = this.fieldData[index];
            this.fieldData[index] = tmp;
            this._fieldsService.gotoRowsUpate(this.fieldData[index].filId, this.fieldData[index + 1].filId, 3).subscribe(
                res => {
                    this.ngOnInit();
                }
            )
        }
    }
}