import { Component,OnInit, ViewEncapsulation, ViewContainerRef, AfterViewInit, AfterContentInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { Modal } from 'angular2-modal/plugins/bootstrap';
import { ModalModule } from "ng2-modal";
import { AppConfig } from "../../app.config";
import { CategoriesService } from '../categories.service';

  
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';
declare var swal: any;
declare var sweetAlert: any;

import { ImageCropperComponent, CropperSettings, Bounds } from 'ng2-img-cropper';
import { PagesComponent } from '../../pages/pages.component';
import { forEach } from '@angular/router/src/utils/collection';
import { ResourceLoader } from '@angular/compiler';

@Component({
    selector: 'categories',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./categories.component.scss'],
    templateUrl: './categories.component.html',
    providers: [CategoriesService]
})

export class CategoriesComponent implements OnInit {
    public rowsOnPage = 10;
    public p :number;
    public total : any;
    config: any;
    configFn: any;
    public category: any = [];
    Dmsg: any = false;
    Amsg: any = false;
    msg: any = false;
    msgDe: any = false;
    public count: any;
    dltcategory: any;
    addCategory: FormGroup;
    editCategory: FormGroup;
    public obj = '';
    public url: any;
    oldName: any;
    newName: any;
    data: any;
    state = 1;
    logoUrl: any;
    activeImg: any;
    deactiveImg: any;
    // editCat:any;
    data1: any;
    dataURI: any;
    public_id: any;
    errorImg = false;
    Aimage: any = '';
    public images: string[] = [];
    btnFlag = true;
    langData = [];
    deActiveCat = [];
    constructor(private changeDetectorRef: ChangeDetectorRef, private _appConfig: AppConfig, private _categoryservice: CategoriesService, private router: Router, vcRef: ViewContainerRef, fb: FormBuilder, public _isAdmin: PagesComponent) {
        this.config = this._appConfig.config;
        this.configFn = this._appConfig;
        this.addCategory = fb.group({
            // 'mainUrl': "",
            'categoryName': ['', Validators.required],
        });
        this.editCategory = fb.group({
            'categoryName': ['', Validators.required],
            'mainUrl': ""
        })
    }

    ngOnInit() {
      //  console.log("the pagenation outside admin", this.rowsOnPage)
        if (this._isAdmin.isAdmin == false) {
           // console.log("the pagenation in admin", this.rowsOnPage)

            var role = sessionStorage.getItem('role');
            var roleDt = JSON.parse(role);
            console.log("role-----",roleDt);

            for (var x in roleDt) {
                if (x == 'categories') {
                    if (roleDt[x] == 0) {
                        this.router.navigate(['error']);
                    } else if (roleDt[x] == 100) {
                        jQuery(".buttonCus").hide();
                        jQuery(".thAction").remove();
                        // jQuery(".thTitle").css("width", "61%");
                        this.btnFlag = false;
                    } else if (roleDt[x] == 110) {
                        this.btnFlag = false;
                        jQuery(".thAction").remove();
                        // jQuery(".thTitle").css("width", "61%");
                    }
                }
            }
        }


        jQuery("#upImg").hide();
        jQuery("#eupImg").hide();

        this._categoryservice.getCategory().subscribe(
            result => {
                 console.log("acres", result)
                if (result.data && result.data.length > 0) {
                    this.msg = false;
                    this.category = result.data;
                    this.count = result.subcategoryname;
                    this.total= result.data.count;
                    this.p = 1;

                } else {
                    this.category = [];
                    this.msg = true;
                }
            }
        )

        this._categoryservice.getDeActiveCat().subscribe(
            res => {
                 console.log("deres", res);
                if (res.data && res.data.length > 0) {
                    this.msgDe = false;
                    this.deActiveCat = res.data;
                    this.total= res.data.count;
                    this.p = 1;


                } else {
                    this.deActiveCat = [];
                    this.msgDe = true;
                }
            }
        )

        this._categoryservice.getLanguage().subscribe(
            res => {
                // console.log("res", res);
                if (res.data && res.data.length > 0) {
                    this.langData = res.data;
                } else {
                    this.langData = [];
                }
            }
        )

    }
    AfileChange(input) {
        if ((input.files[0].size / 1000) > 300) {
            jQuery("#upImg").show();
            jQuery("#eupImg").show();
        } else {
            jQuery("#upImg").hide();
            jQuery("#eupImg").hide();
            const reader = new FileReader();
            if (input.files.length) {
                const file = input.files[0];
                reader.onload = () => {
                    this.Aimage = reader.result;
                }
                reader.readAsDataURL(file);
            }
        }
    }
    AremoveImage(): void { this.Aimage = ''; }
    submitForm(value) {
        console.log("data cat------",value);
        var otherName = [];
        this.langData.forEach(e => {
            let x = {
                langId: e.langId,
                catName: jQuery(".cat" + e.langId).val()
            }
            otherName.push(x);
        });
        if (this.Aimage.length == 0) {
            this.Amsg = true;
        } else {
            this.Dmsg = false;
            this.Amsg = false;
            value._value.activeimage = this.Aimage;
            value._value.otherName = otherName;
            // console.log("asd", value._value);
            this._categoryservice.addCategory(value._value).subscribe(
                result => {
                    // console.log("result", result);
                    if (result.code == 200) {
                        swal("Good job!", "Category Added Successfully!", "success");
                    } else {
                        sweetAlert("Oops...", "Something went wrong!", "error");
                    }
                    this.images = [];
                    jQuery('#addCategory').modal('hide');
                    this.ngOnInit();
                    this.addCategory.reset();
                }
            )
        }
    }

    //depricated
    submitData(url) {
        this.data.mainUrl = url;
        // console.log("data", this.data);
        this._categoryservice.addCategory(this.data).subscribe(
            result => {
                // console.log("add categories", result);
                if (result.code == 200) {
                    swal("Good job!", "Category Added Successfully!", "success");
                } else {
                    sweetAlert("Oops...", "Something went wrong!", "error");
                }

                jQuery('#addCategory').modal('hide');
                this.ngOnInit();
                this.addCategory.reset();
            }
        )
    }
    editCategoryData(category) {
        jQuery(".xyz").val('');
        console.log("data", category);
        this.obj = category;
        this.oldName = category.name;
        jQuery('#newName').val(this.oldName);
        this.logoUrl = category.categoryImageUrl;
        this.activeImg = category.activeimage;
        this.Aimage = category.activeimage;
        this.langData.forEach(ele => {
            category.catOtherName.forEach(element => {
                if (ele.langId == element.langId) {
                    jQuery(".editCat" + ele.langId).val(element.otherName);
                }
            });
        });

    }
    updatecategory(subCategory) {
        var otherName = [];
        this.langData.forEach(e => {
            let x = {
                langId: e.langId,
                catName: jQuery(".editCat" + e.langId).val()
            }
            otherName.push(x);
        });
        // subCategory._value.otherName = otherName;
        console.log("asd", subCategory._value);
        this.newName = jQuery("input#newName").val();
        if (this.Aimage.length == 0) {
            this.Amsg = true;
        } else {
            this.Amsg = false;
            this._categoryservice.editCategory(this.oldName, this.newName, this.activeImg, this.Aimage, otherName).subscribe(
                result => {
                    // console.log("result", result);
                    if (result.code == 200) {
                        swal("Good job!", "Category Edited Successfully!", "success");
                    } else {
                        sweetAlert("Oops...", "Something went wrong!", "error");
                    }
                    this.images = [];
                    jQuery('#editCategory').modal('hide');
                    this.ngOnInit();
                    this.addCategory.reset();
                }
            )
        }
        // this.state = 0;
        // this.newName = jQuery("input#newName").val();
        // let photo = jQuery("#editphoto").val();
        // if (photo) {
        //     // this.uploader.uploadAll();
        // } else {
        //     this.editCatData(this.logoUrl);
        // }
        // this._categoryservice.editCategory(this.oldName, this.newName).subscribe(
        //     result => {
        //         console.log("result", result);
        //         jQuery('#editCategory').modal('hide');
        //         this.ngOnInit();
        //     }
        // )

    }
    // editCatData(url) {
    //     console.log("oldName", this.oldName);
    //     console.log("newName", this.newName);
    //     console.log("url", url);
    //     this._categoryservice.editCategory(this.oldName, this.newName, url).subscribe(
    //         result => {
    //             console.log("result", result);
    //             jQuery('#editCategory').modal('hide');
    //             this.ngOnInit();
    //         }
    //     )
    // }



    gotoSubcate(categoryName: any) {
        console.log("categories", categoryName.toString());
        this.router.navigate(['/app/categories/sub-categories', categoryName]);
    }
    gotoFilterPage(categoryName) {
        this.router.navigate(['/app/categories/field', categoryName]);
    }
    gotoDeleteCategory(category) {
        var cat = this;
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Category!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
            function (isConfirm) {
                if (isConfirm) {
                    cat.deletecategory(category);
                    swal({
                        title: 'Delete!',
                        text: 'Category Deleted Successfully!',
                        type: 'success'
                    });

                } else {
                    swal("Cancelled", "Your Category is safe :)", "error");
                }
            });
    }

    deletecategory(category) {
        // console.log("abc", category)
        this._categoryservice.deletecategory(category).subscribe(
            result => {
                // console.log("result", result);
                // if (result.code != 200) {
                //     sweetAlert("Oops...", "Something went wrong!", "error");
                // }
                // this.dltcategory = result;
                this.ngOnInit();
            }
        )
    }
    clearModal() {
        this.Aimage = '';
        this.addCategory.reset();
    }
    gotoActiveCategory(categoryName) {
        this._categoryservice.gotoActiveCategory(categoryName).subscribe(
            res => {
                console.log("res", res);
                if (res.code == 200) {
                    this.ngOnInit();
                    swal("Success!", "Category Active Successfully!", "success");
                }
            }
        )
    }
    gotoDeActiveCategories(deActivecategory) {

        var cat = this;
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this DeActivecategory!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
            function (isConfirm) {
                if (isConfirm) {
                    cat.deleteDeactivecategory(deActivecategory);
                    swal({
                        title: 'Delete!',
                        text: 'DeActivecategory Deleted Successfully!',
                        type: 'success'
                    });

                } else {
                    swal("Cancelled", "Your DeActivecategory is safe :)", "error");
                }
            });
    }

    deleteDeactivecategory(deActivecategory) {
        console.log("abc", deActivecategory)
        this._categoryservice.deleteDeActivecategory(deActivecategory).subscribe(
            result => {
                // console.log("result", result);
                // if (result.code != 200) {
                //     sweetAlert("Oops...", "Something went wrong!", "error");
                // }
                // this.dltcategory = result;
                this.ngOnInit();
            }
        )
    }
    gotoGetData() {
        jQuery('.buttonCus').show();
        this.ngOnInit();
    }
    gotoGetDeAciveData() {
        jQuery('.buttonCus').hide();
        this.ngOnInit();
    }

    gotoUpArrow(index) {
         console.log("up arrow", index);
        if (index > 0) {
            const tmp = this.category[index - 1];
            // console.log("upId", this.category[index - 1].catId)
            // console.log("currentId", this.category[index].catId)
            this.category[index - 1] = this.category[index];
            this.category[index] = tmp;
            this._categoryservice.gotoRowsUpate(this.category[index].catId, this.category[index - 1].catId, 1).subscribe(
                res => {
                    // console.log("res", res);
                    this.ngOnInit();
                }
            )
        }
        // console.log(this.category)
    }
    gotoDownArrow(index) {
     console.log("down arrow", index);
        if (index < this.category.length) {
            const tmp = this.category[index + 1];
            this.category[index + 1] = this.category[index];
            this.category[index] = tmp;
            // console.log("downId", this.category[index + 1].catId, this.category[index + 1].name)
            // console.log("currentId", this.category[index].catId, this.category[index].name)
            this._categoryservice.gotoRowsUpate(this.category[index].catId, this.category[index + 1].catId, 1).subscribe(
                res => {
                    // console.log("res", res);
                    this.ngOnInit();
                }
            )
        }
    }
    pageChanged($event){
      console.log("the current page is ", event);
         
    //   let currentPage = event;
    //   this.rowsOnPage = Number(currentPage);
    //   console.log("the current page is ", this.rowsOnPage);
    //   this.p = Number(currentPage);
    }
}